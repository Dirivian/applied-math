# README #


### What is this repository for? ###

This repository houses a lot of code I wrote for various Applied Math courses during my Masters at the University of Washington.



Most of the code is undocumented. However, there are a lot of pdfs in each folder that offer both mathematical insight as well as insight into the code.