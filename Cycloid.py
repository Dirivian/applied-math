# -*- coding: utf-8 -*-
"""
Created on Wed Jan  4 16:42:00 2017

@author: user
"""
#plots a cycloid

import numpy as np
import matplotlib.pyplot as plt

t = np.linspace(0,3*np.pi,1000)
i=0
a=30
R=3
y= np.zeros(1000)
x=np.zeros(1000)
for theta in t:
    y[i]= -R*(1-np.cos(theta))
    x[i] = R*(theta- np.sin(theta))
    i+=1
plt.xlim( (0,25) )
plt.ylim( (-10,0) )
plt.plot(x,y)  
plt.show()  